<?php

namespace Drupal\matrix_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Matrix Field entities.
 */
interface MatrixFieldInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
