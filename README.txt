Provides multi-valued field, that contains items with labels, units and values. Sets of labels and units are united into matrices, that you can choose in field widget. When you select another matrix, set of fields changes.

Example of usage:
When you create commerce store, that offers some technical products, you need very big amount of fields with technical characteristics and also big amount of product types. With this module you need only one product type and only one field. Just create matrices and matrix fields and select in product appropriate matrix to load only needed matrix fields.

Features
Integration with Search API. You can create facets for any matrix field
Why not stable release?
Leak of documentation
No views integration
This will be fixed soon.

By the way, module is already ready for usage on production sites. It was successfully tested at a couple of live sites.
